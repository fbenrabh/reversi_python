# Othello

Cette application réalisé en Python permet que l’utilisateur joue contre un IA  ou un IA contre IA en utilisant l’algorithme MinMax ou l’algorithme alpha-bêta .

## Description du jeu 
Othello est un jeu opposant deux adversaires de part et d'autre d'une grille unicolore de 64cases, pour laquelle les adeptes français ont forgé le néologisme «othellier». Les joueurs disposent de 64 pions bifaces, noirs d'un côté et blancs de l'autre. Au départ, chacun en a 32, mais comme ils sont identiques et tous réversibles, ils sont mis en commun. En fait, c'est la couleur de la face qui appartient à un des joueurs: l'un a les noirs, l'autre les blancs.

## But de jeu 
Le but du jeu est de s'assurer le maximum de faces de sa couleur en «capturant» les pions adverses, qui sont alors retournés. 
La capture s'effectue selon le mode dit en tenaille, c'est-à-dire en plaçant un pion de sa couleur de part et d'autre d'un pion ou d'un alignement de la couleur opposée. Trois pions noirs encadrés à chaque extrémité par un pion blanc sont ainsi «pris» et deviennent blancs. Mais rien n'empêche le joueur «noir», qui se trouverait en position de le faire, de retourner à son profit tous ces pions et les deux siens, renversant ainsi la situation. Quand un joueur ne peut poser de pion ni retourner un pion adverse, il doit passer. Le retournement est obligatoire. Lorsque les deux joueurs se trouvent en situation de passer, la partie est terminée. C'est le joueur qui a le plus de pions de sa couleur qui a gagné.

## Pour tester l’application 
Pour tester l’application il faut importer jeulib ensuite créer un objet J et demarer le jeu à l’aide de la fonction demarer .
import jeulib as jeu 
J= jeu.Jeu()
J.demarrer()

## Description du code 
Au début du projet j’ai commencé par coder une fonction Random qui permet de choisir un coup aléatoire de la liste des coups qui sont valides et joue ce coup 

## Algorithme Minmax 
Le but de la partie Min est de trouver le minimum des nœuds-fils du nœud envoyé en paramètre.Or chaque nœud-fils est le maximum de ses propres nœuds-fils. Il faut donc s’arrêter au bon moment.
Pour la partie Max elle est en tout point semblable à la fonction Min, sauf qu'au lieu de chercher le minimum de ses nœuds-fils, elle en cherche le maximum 
en testant l’algorithme MinMax contre le random le random perd dans le jeu et en augmentant la profondeur la difficulté du jeu augmente 

## Algorithme alpha-beta 
l’algorithme alpha-beta permet d'optimiser grandement l'algorithme minimax sans en modifier le résultat. Pour cela, il ne réalise qu'une exploration partielle de l'arbre. Lors de l'exploration, il n'est pas nécessaire d'examiner les sous-arbres qui conduisent à des configurations dont la valeur ne contribuera pas au calcul du gain à la racine de l'arbre

## Différence entre alpha beta 
L'algorithme alpha-beta n’est qu’une optimisation du MiniMax, qui permet de couper  des sous arbres dès que leur valeur devient inintéressante aux fins du calcul de la valeur MiniMax du jeu. On s'intéressera donc, sur chaque noeud, en plus de la valeur, à deux autres quantités, nommées alpha et beta, qui sont utilisées ensuite pour calculer la valeur du noeud.


