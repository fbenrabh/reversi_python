#!/usr/bin/python
# coding: utf8
import time
import interfacelib
import numpy as np


class Joueur:
	def __init__(self, partie, couleur, opts={}):
		self.couleur = couleur
		self.jeu = partie
		self.opts = opts

	def demande_coup(self):
		pass


class Humain(Joueur):

	def demande_coup(self):
		pass



class IA(Joueur):

	def __init__(self, partie, couleur, opts={}):
		super().__init__(partie,couleur,opts)
		self.temps_exe = 0
		self.nb_appels_jouer = 0
		
		
# fonction pour random 	
class Random(IA):

	def __init__(self, partie, couleur, opts={}):
		super().__init__(partie,couleur,opts)

	def demande_coup(self):
		couleurval = interfacelib.couleur_to_couleurval(self.couleur)
		listeCoups = self.jeu.plateau.liste_coups_valides(couleurval)
		if(listeCoups==[]):
			return []
		indice = (int) (np.random.random_integers(0,len(listeCoups)-1,1))
		return listeCoups[indice]

# fonction pour min_max 
class MinMax(IA):
	
	iteratif = 0

	def __init__(self, partie, couleur, opts={}):
		super().__init__(partie,couleur,opts)

	def demande_coup(self):
		time1 = time.time()
		couleurval = interfacelib.couleur_to_couleurval(self.couleur)
		listeCoups = self.jeu.plateau.liste_coups_valides(couleurval)
		Max = True
		profondeurAlgo = 3
		resultat = -65
		nextCoup =[]
		if(listeCoups==[]):
			return []
			
		copiePlateau = self.jeu.plateau.copie()
		for coup in listeCoups:
			copiePlateau.jouer(coup,couleurval)
			self.iteratif+=1	
			temp = self.minmax(profondeurAlgo-1,copiePlateau, not Max)
			copiePlateau = self.jeu.plateau.copie()
			if(temp > resultat or temp == resultat):
				resultat=temp
				nextCoup= coup
		
		time2 = time.time()
		return nextCoup
		
	def evaluer(self,plateau):
		score_noir=0
		score_blanc=0
		for i in range(8):
			for j in range(8):
				if(plateau.tableau_cases[i][j]==1):
					score_noir+=1
				elif(plateau.tableau_cases[i][j]==-1):
					score_blanc+=1
							
		if(self.couleur=='noir'):						
			return score_noir-score_blanc
		else:
			return score_blanc-score_noir			
		
					
	def minmax(self,profondeurAlgo,plateau,Max):
		if (profondeurAlgo==0 ):
			return self.evaluer(plateau)

		couleurval = interfacelib.couleur_to_couleurval(self.couleur)
		listeCoups = plateau.liste_coups_valides(couleurval)

		if (Max) :
			resultat = -65
			copiePlateau = plateau.copie()	
			for coup in listeCoups:
				copiePlateau.jouer(coup,couleurval)	
				self.iteratif+=1	
				temp = self.minmax(profondeurAlgo-1,copiePlateau,not Max)
				copiePlateau = plateau.copie()
				resultat = max(temp,resultat)
      
			return resultat
		else :
			resultat = 65
			copiePlateau = plateau.copie()
			for coup in listeCoups:
				copiePlateau.jouer(coup,couleurval)	
				self.iteratif+=1	
				temp = self.minmax(profondeurAlgo-1,copiePlateau, Max)
				copiePlateau = plateau.copie()
				resultat = min(temp,resultat)
      
			return resultat
		  
      


class AlphaBeta(IA):
	
	iteratif = 0

	def __init__(self, partie, couleur, opts={}):
		super().__init__(partie,couleur,opts)

	def demande_coup(self):
		time1 = time.time()
		couleurval = interfacelib.couleur_to_couleurval(self.couleur)
		listeCoups = self.jeu.plateau.liste_coups_valides(couleurval)
		Max = True
		profondeurAlgo = 3
		resultat = -65
		nextCoup =[]
		if(listeCoups==[]):
			return []
			
		copiePlateau = self.jeu.plateau.copie()
		for coup in listeCoups:
			copiePlateau.jouer(coup,couleurval)
			self.iteratif+=1	
			temp = self.alpha_beta(profondeurAlgo-1,copiePlateau,-65,65,not Max)
			copiePlateau = self.jeu.plateau.copie()
			if(temp > resultat or temp==resultat):
				resultat=temp
				nextCoup= coup
		
		time2 = time.time()		
		return nextCoup
		
	def evaluer(self,plateau):
		score_noir=0
		score_blanc=0
		for i in range(8):
			for j in range(8):
				if(plateau.tableau_cases[i][j]==1):
					score_noir+=1
				elif(plateau.tableau_cases[i][j]==-1):
					score_blanc+=1		
		
		if(self.couleur=='noir'):						
			return score_noir-score_blanc
		else:
			return score_blanc-score_noir	
	
	
					
	def alpha_beta(self,profondeurAlgo,plateau,alpha,beta,Max):
		if (profondeurAlgo==0 ):
				return self.evaluer(plateau)

		couleurval = interfacelib.couleur_to_couleurval(self.couleur)
		listeCoups = plateau.liste_coups_valides(couleurval)

		if (Max) :
			resultat = -65
			copiePlateau = plateau.copie()	
			for coup in listeCoups:
				copiePlateau.jouer(coup,couleurval)	
				self.iteratif+=1	
				val = self.alpha_beta(profondeurAlgo-1,copiePlateau,alpha,beta,not Max)
				copiePlateau = plateau.copie()
				resultat = max(val,resultat)
				alpha = max(alpha,resultat)
				if(beta <= alpha):
					return resultat
            
      
			return resultat
		else :
			resultat = 65
			copiePlateau = plateau.copie()
			for coup in listeCoups:
				copiePlateau.jouer(coup,couleurval)	
				self.iteratif+=1	
				val = self.alpha_beta(profondeurAlgo-1,copiePlateau,alpha,beta, Max)
				copiePlateau = plateau.copie()
				resultat = min(val,resultat)
				beta = min(alpha,resultat)
				if(beta <= alpha):
					return resultat
      
			return resultat
